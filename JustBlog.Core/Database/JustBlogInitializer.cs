﻿using JustBlog.Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Core.Database
{
    public static class JustBlogInitializer
    {
        public static void SeedData(this ModelBuilder builder)
        {
            List<Category> categories = new List<Category>();
            List<Post> posts = new List<Post>();
            List<Tag> tags = new List<Tag>();
            List<PostTagMap> postTags = new List<PostTagMap>();
            List<Comment> comments = new List<Comment>();

            // Categories
            for (int i = 1; i <= 10; i++)
            {
                categories.Add(new Category
                {
                    Id = i,
                    Name = LoremNET.Lorem.Words(2),
                    UrlSlug = LoremNET.Lorem.Words(2).ToLower().Replace(' ', '-'),
                    Description = LoremNET.Lorem.Words(12)
                });
            }

            // Posts
            for (int i = 1; i <= 10; i++)
            {
                posts.Add(new Post
                {
                    Id = i,
                    Title = LoremNET.Lorem.Words(3),
                    UrlSlug = LoremNET.Lorem.Words(3).ToLower().Replace(' ', '-'),
                    ShortDescription = LoremNET.Lorem.Words(13),
                    PostContent = LoremNET.Lorem.Paragraph(10, 20, 4, 8),
                    PostedOn = LoremNET.Lorem.DateTime(DateTime.Now.AddDays(-10), DateTime.Now),
                    Published = true,
                    CategoryId = LoremNET.RandomHelper.Instance.Next(1, 6),
                    RateCount = LoremNET.RandomHelper.Instance.Next(10, 30),
                    TotalRate = LoremNET.RandomHelper.Instance.Next(100, 300),
                    ViewCount = LoremNET.RandomHelper.Instance.Next(100, 300),
                });
            }

            // Tags
            for (int i = 1; i <= 10; i++)
            {
                tags.Add(new Tag
                {
                    Id = i,
                    Name = LoremNET.Lorem.Words(3),
                    UrlSlug = LoremNET.Lorem.Words(3).ToLower().Replace(' ', '-'),
                    Description = LoremNET.Lorem.Words(13),
                    Count = LoremNET.RandomHelper.Instance.Next(10, 30)
                });
            }

            // PostTags
            for (int i = 1; i <= 10; i++)
            {
                postTags.Add(new PostTagMap
                {
                    PostId = posts[i-1].Id,
                    TagId = tags[i-1].Id
                });
            }

            // Comments
            for (int i = 1; i <= 10; i++)
            {
                comments.Add(new Comment
                {
                    Id = i,
                    CommentHeader = LoremNET.Lorem.Words(3),
                    CommentText = LoremNET.Lorem.Paragraph(10, 20, 1, 3),
                    CommentTime = LoremNET.Lorem.DateTime(DateTime.Now.AddDays(-10), DateTime.Now),
                    Email = LoremNET.Lorem.Email(),
                    Name = LoremNET.Lorem.Words(2),
                    PostId = LoremNET.RandomHelper.Instance.Next(1, 10),
                });
            }

            // Gallery Images
            builder.Entity<GalleryImage>().HasData(
                new GalleryImage
                {
                    Id = 1,
                    Title = "Learn C#",
                    Description = "Introduction programming C#",
                    ImageUrl = "~/assets/img/Introduction-to-C.jpg",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "Tran Van Hanh",
                    PostId = LoremNET.RandomHelper.Instance.Next(1, 10)
                },
                new GalleryImage
                {
                    Id = 2,
                    Title = "Football World Cup",
                    Description = "Messi Winner Football World Cup 2022",
                    ImageUrl = "~/assets/img/footballworlcup.jpg",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "Fabrizio Romano",
                    PostId = LoremNET.RandomHelper.Instance.Next(1, 10)
                },
                new GalleryImage
                {
                    Id = 3,
                    Title = "Introduction to Programming",
                    Description = "Learn basic",
                    ImageUrl = "~/assets/img/programming.jpg",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "Anh Quan",
                    PostId = LoremNET.RandomHelper.Instance.Next(1, 10)
                },
                new GalleryImage
                {
                    Id = 4,
                    Title = "WEB trending",
                    Description = "Supper WEB",
                    ImageUrl = "~/assets/img/Web-Development-Trends.jpg",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "Ta Van Toan",
                    PostId = LoremNET.RandomHelper.Instance.Next(1, 10)
                },
                new GalleryImage
                {
                    Id = 5,
                    Title = "Ha Long Bay",
                    Description = "Description for Image 1",
                    ImageUrl = "~/assets/img/Halong-Main-picture.jpg",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "Pham Thi Len",
                    PostId = LoremNET.RandomHelper.Instance.Next(1, 10)
                },
                new GalleryImage
                {
                    Id = 6,
                    Title = "Food in Viet Nam",
                    Description = "Supper likes",
                    ImageUrl = "~/assets/img/food_3.jpg",
                    CreatedDate = DateTime.Now,
                    CreatedBy = "Tran Huy Tho",
                    PostId = LoremNET.RandomHelper.Instance.Next(1, 10)
                }
            );

            //Roles
            var roles = new IdentityRole[]
            {
                new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Blog Owner",
                    NormalizedName = "Blog Owner".ToUpper()
                },
                new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Contributor",
                    NormalizedName = "Contributor".ToUpper()
                },
                new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "User",
                    NormalizedName = "User".ToUpper()
                },
                new IdentityRole
                {
                Id = Guid.NewGuid().ToString(),
                Name = "Admin",
                NormalizedName = "ADMIN"
                }
            };

            //Users
            var users = new User[]
            {
                new User
                {
                    Id = Guid.NewGuid().ToString(),
                    Age = LoremNET.RandomHelper.Instance.Next(20, 60),
                    AboutMe = LoremNET.Lorem.Words(10),
                    UserName = "anhquan18",
                    NormalizedUserName = "anhquan18".ToUpper(),
                    Email = "anhquan18@gmail.com",
                    NormalizedEmail = "anhquan18@gmail.com".ToUpper(),
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    PasswordHash = new PasswordHasher<User>().HashPassword(null!, "Anhquan18")
},
                new User
                {
                    Id = Guid.NewGuid().ToString(),
                    Age = LoremNET.RandomHelper.Instance.Next(20, 60),
                    AboutMe =  LoremNET.Lorem.Words(10),
                    UserName = "hanhtran24",
                    NormalizedUserName = "hanhtran24".ToUpper(),
                    Email = "hanhtran24@gmail.com",
                    NormalizedEmail = "hanhtran24@gmail.com".ToUpper(),
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    PasswordHash = new PasswordHasher<User>().HashPassword(null!, "Hanhtran24")
                },
                new User
                {
                    Id = Guid.NewGuid().ToString(),
                    Age = LoremNET.RandomHelper.Instance.Next(20, 60),
                    AboutMe =  LoremNET.Lorem.Words(10),
                    UserName = "nhatminhk65",
                    NormalizedUserName = "nhatminhk65".ToUpper(),
                    Email = "nhatminhk65@gmail.com",
                    NormalizedEmail = "nhatminhk65@gmail.com".ToUpper(),
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    PasswordHash = new PasswordHasher<User>().HashPassword(null!, "Nhatminhk65")
                },
            };

            //UserRoles
            var userRoles = new IdentityUserRole<string>[]
            {
                new IdentityUserRole<string>
                {
                    RoleId = roles[0].Id,
                    UserId = users[0].Id
                },
                new IdentityUserRole<string>
                {
                    RoleId = roles[1].Id,
                    UserId = users[1].Id
                },
                new IdentityUserRole<string>
                {
                    RoleId = roles[2].Id,
                    UserId = users[2].Id
                }
            };

            //Seed data
            builder.Entity<Category>().HasData(categories);
            builder.Entity<Post>().HasData(posts);
            builder.Entity<Tag>().HasData(tags);
            builder.Entity<PostTagMap>().HasData(postTags);
            builder.Entity<Comment>().HasData(comments);
            builder.Entity<User>().HasData(users);
            builder.Entity<IdentityRole>().HasData(roles);
            builder.Entity<IdentityUserRole<string>>().HasData(userRoles);
        }
    }
}