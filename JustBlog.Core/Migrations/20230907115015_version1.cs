﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace JustBlog.Core.Migrations
{
    public partial class version1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1026)", maxLength: 1026, nullable: false),
                    Count = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Age = table.Column<int>(type: "int", nullable: false),
                    AboutMe = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    ShortDescription = table.Column<string>(name: "Short Description", type: "nvarchar(500)", maxLength: 500, nullable: false),
                    PostContent = table.Column<string>(name: "Post Content", type: "nvarchar(4000)", maxLength: 4000, nullable: false),
                    UrlSlug = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Published = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    ViewCount = table.Column<int>(name: "View Count", type: "int", nullable: false, defaultValue: 0),
                    RateCount = table.Column<int>(name: "Rate Count", type: "int", nullable: false, defaultValue: 0),
                    TotalRate = table.Column<int>(name: "Total Rate", type: "int", nullable: false, defaultValue: 0),
                    PostedOn = table.Column<DateTime>(name: "Posted On", type: "datetime2", nullable: false),
                    Modified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CategoryId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    CommentHeader = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    CommentText = table.Column<string>(type: "nvarchar(1026)", maxLength: 1026, nullable: false),
                    CommentTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GalleryImages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GalleryImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GalleryImages_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostTagMaps",
                columns: table => new
                {
                    PostId = table.Column<int>(type: "int", nullable: false),
                    TagId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostTagMaps", x => new { x.PostId, x.TagId });
                    table.ForeignKey(
                        name: "FK_PostTagMaps_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PostTagMaps_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Description", "Name", "UrlSlug" },
                values: new object[,]
                {
                    { 1, "Mauris platea ac taciti neque eros purus ante amet ornare varius sagittis", "Varius odio", "cursus-dictum" },
                    { 2, "Bibendum lacinia commodo et nunc ultrices feugiat sed lorem rutrum et nisi", "Odio diam", "nisi-nec" },
                    { 3, "Dui sagittis bibendum porta sapien ex magna commodo ultricies non viverra varius", "Lorem sed", "aenean-aptent" },
                    { 4, "Interdum dictumst augue iaculis tortor mauris convallis velit quam vehicula id maecenas", "Nunc morbi", "tellus-maecenas" },
                    { 5, "Leo congue sit ultrices vulputate enim at hendrerit nisi nulla laoreet class", "Tortor volutpat", "interdum-tortor" },
                    { 6, "Ultrices varius feugiat pharetra integer curabitur volutpat urna arcu ipsum euismod praesent", "Eros nisi", "donec-mauris" },
                    { 7, "Ex nibh morbi sagittis suscipit mi sociosqu accumsan convallis nibh dolor ut", "Mi feugiat", "praesent-sagittis" },
                    { 8, "Elit vivamus mattis finibus sapien nibh a lorem dictum pellentesque porttitor dolor", "Vestibulum amet", "orci-habitasse" },
                    { 9, "At posuere eros tempor aenean commodo imperdiet consectetur volutpat enim id quam", "Maecenas justo", "nisi-mollis" },
                    { 10, "Lorem et ultricies dictum laoreet etiam eleifend tortor elit ligula laoreet cursus", "Eget pellentesque", "risus-interdum" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "0cdf28f2-e109-4360-9c5e-b8a4df66091e", "b0bbfe9f-350b-496e-80fe-2c4e10a4c92c", "User", "USER" },
                    { "5833bba7-8820-4163-b779-b36d239ad574", "090ef43e-c4bc-42f8-a3c2-88b146b55c3f", "Contributor", "CONTRIBUTOR" },
                    { "a434f075-570e-44f9-82f8-62cd88f5eb0a", "caea967b-3877-4eae-be91-2488a9dbb404", "Blog Owner", "BLOG OWNER" },
                    { "eda7a356-26ee-47ab-8fe2-43273e8b5130", "fd7ca6e5-69a7-4693-9285-f32e5e56ae30", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Count", "Description", "Name", "UrlSlug" },
                values: new object[,]
                {
                    { 1, 17, "Et tellus mollis vitae etiam tempor ligula ut luctus ante purus varius orci", "Cursus lorem sem", "sed-eu-leo" },
                    { 2, 15, "Id viverra sagittis quam porttitor vivamus etiam amet posuere eget ad inceptos non", "Mattis turpis hac", "odio-praesent-integer" },
                    { 3, 28, "Nibh adipiscing bibendum aliquet aenean at nec nulla magna odio vel leo sociosqu", "Porta mattis eleifend", "laoreet-magna-congue" },
                    { 4, 15, "Nibh tempus enim posuere consectetur libero eros class volutpat nec himenaeos elit mi", "Sit ante id", "aptent-platea-lectus" },
                    { 5, 24, "Orci metus tellus maximus ad nibh porta adipiscing dui nostra interdum sem blandit", "Erat consequat nunc", "inceptos-ullamcorper-mollis" },
                    { 6, 27, "Egestas curabitur elit ut neque facilisis torquent ornare auctor nullam tincidunt dignissim sollicitudin", "Odio massa amet", "vel-dictum-elit" },
                    { 7, 16, "Hendrerit quis quisque consectetur eu mi est pulvinar tellus malesuada rhoncus odio tempor", "Luctus lorem tempus", "scelerisque-augue-dui" },
                    { 8, 29, "Luctus mi rhoncus sed lectus nibh nulla magna sed commodo nunc odio laoreet", "Scelerisque consequat ipsum", "vestibulum-facilisis-iaculis" },
                    { 9, 12, "Nisi aliquet sagittis rhoncus posuere morbi elit massa et iaculis at mauris libero", "Mauris neque ad", "sed-lorem-a" },
                    { 10, 20, "Ultrices laoreet eleifend auctor finibus arcu viverra auctor proin dignissim in nibh justo", "Rhoncus at sagittis", "erat-euismod-maximus" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AboutMe", "AccessFailedCount", "Age", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "733a30f4-82e2-4302-b3d4-d0e736d746ed", "Ex tortor enim volutpat lectus efficitur vulputate vel accumsan maximus", 0, 33, "624a642b-7133-4390-8b6f-fd726dcf9bfb", "hanhtran24@gmail.com", true, false, null, "HANHTRAN24@GMAIL.COM", "HANHTRAN24", "AQAAAAEAACcQAAAAEPIbpEGw9tQaCzXw//1B03vew1m0E0L19adDUdKliLmAdr5MkCSf0xHrER3Cl47zaw==", null, false, "fac1df82-5804-46c4-b0c0-d4c024f2bcbc", false, "hanhtran24" },
                    { "85f7dca3-ed32-49e7-bf16-f49e3bb8ae04", "Id eget magna egestas maecenas quam vestibulum lorem vel conubia", 0, 47, "7632a939-0094-49cd-ae9d-1f3cff7b6f5a", "nhatminhk65@gmail.com", true, false, null, "NHATMINHK65@GMAIL.COM", "NHATMINHK65", "AQAAAAEAACcQAAAAEKJM24qBVO0WbvQmhYKEVX0A5a8zMJ2cAPlzIo0ohX1742pYNxgY3keTjAufHGZPhQ==", null, false, "6aeb54df-00c8-4187-856e-901ca26594dd", false, "nhatminhk65" },
                    { "a9d1613a-6655-4ff5-bedf-313bf30093c1", "Diam lacus scelerisque nunc ac aenean class tortor ac porttitor", 0, 31, "8897db17-0b0f-4a29-a06e-399961836bba", "anhquan18@gmail.com", true, false, null, "ANHQUAN18@GMAIL.COM", "ANHQUAN18", "AQAAAAEAACcQAAAAEDnfOYJQUp3Jqj92oaMkDE7aKwwVAYiqZ2aMSlSjmc2FpBeHpigN3Br7C26m8Y55eg==", null, false, "c4e4dd52-4fa2-48b6-8185-ecd344f1429b", false, "anhquan18" }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "CategoryId", "Modified", "Post Content", "Posted On", "Published", "Rate Count", "Short Description", "Title", "Total Rate", "UrlSlug", "View Count" },
                values: new object[,]
                {
                    { 1, 4, null, "Luctus laoreet placerat mollis amet eros, dolor nostra, pellentesque mauris sem. Fusce leo nullam sem, sagittis, sed, eleifend maecenas varius, dui. Volutpat mauris hac sociosqu ultricies dui, feugiat et, a, in, ligula mauris, amet ut euismod, iaculis blandit, finibus. Nisi, a, ligula, magna, efficitur pellentesque fames venenatis ultricies est urna imperdiet vivamus. Enim, a sapien dui, quam mattis, amet vehicula quisque vulputate pharetra platea nunc, lectus, erat. Fringilla, cursus condimentum ut duis libero commodo, ac euismod tellus, quisque tortor facilisis nec, posuere, ultricies risus eleifend, feugiat.", new DateTime(2023, 8, 30, 4, 16, 15, 345, DateTimeKind.Local).AddTicks(1663), true, 12, "Vehicula sapien a porta fringilla ullamcorper vestibulum laoreet quam mattis primis volutpat augue", "Porttitor nunc fusce", 297, "suscipit-quam-pharetra", 245 },
                    { 2, 3, null, "Lacinia, tortor, ultrices, leo volutpat, rutrum at, velit sem eros rhoncus, lectus urna aliquet magna, vel. Aenean mauris, eros, fringilla, himenaeos fames congue lectus condimentum nulla, feugiat. Habitasse praesent porttitor, inceptos primis urna duis auctor, pretium enim, rhoncus. Tristique mattis, turpis leo, amet, viverra neque arcu, sapien pulvinar, nisl etiam ac commodo, consectetur rhoncus. Faucibus habitasse fringilla sagittis, nunc, sapien est pulvinar porta, convallis mauris, nulla taciti tellus sollicitudin tortor. Convallis justo sem, mauris taciti molestie hendrerit enim metus nostra, non diam dictum luctus. Pharetra dictum quisque fusce ullamcorper fames vivamus in vel, morbi enim, leo laoreet.", new DateTime(2023, 9, 3, 18, 13, 15, 345, DateTimeKind.Local).AddTicks(3290), true, 14, "Facilisis rhoncus egestas iaculis suscipit lectus a magna fringilla placerat nunc nec malesuada", "Ad pulvinar nec", 291, "cras-nullam-ac", 131 },
                    { 3, 5, null, "Tellus velit ante, pulvinar, ultrices, vulputate dictumst iaculis nibh, blandit, risus euismod, tempor, aenean vehicula. Ut et, posuere, diam primis viverra donec porta, molestie class tincidunt feugiat laoreet urna, vitae maximus auctor, elit. Eros tristique nec tincidunt morbi finibus blandit, primis suspendisse aliquam himenaeos commodo, ultrices, maximus sem tempor magna feugiat. Elementum erat, rutrum eleifend velit mattis non, sem quisque mattis, pharetra vel nisi congue. Quam mattis per nibh platea velit malesuada erat hac aptent inceptos magna tempor, mauris, ullamcorper. Per nam venenatis libero magna consequat tellus suscipit inceptos amet. Lacinia praesent bibendum auctor, quis, sociosqu rhoncus gravida fringilla, class finibus malesuada lobortis nullam magna.", new DateTime(2023, 9, 2, 9, 31, 15, 345, DateTimeKind.Local).AddTicks(4849), true, 24, "Felis lacinia mauris phasellus tempor risus sed blandit adipiscing fringilla mauris congue libero", "Luctus eros finibus", 119, "pulvinar-dui-metus", 139 },
                    { 4, 2, null, "Elit integer felis quis pretium leo, ad accumsan dolor elementum a, metus enim cursus blandit sollicitudin. Lacinia, fringilla semper gravida congue, taciti interdum, nunc, placerat, pharetra vitae. Euismod laoreet libero ornare bibendum volutpat maecenas ultrices, a, morbi ante. Ornare nisi augue eu ad leo, lectus lacinia, mi, nec, venenatis lacinia vivamus dui, a finibus eros. Finibus habitasse fermentum nibh, auctor, euismod, venenatis elit sed, nostra, ligula, ornare condimentum integer mauris cursus varius, imperdiet dapibus. Et ut sit efficitur vitae, in lectus risus sollicitudin posuere congue bibendum, mi, elit id sagittis laoreet, tempor.", new DateTime(2023, 8, 28, 20, 17, 15, 345, DateTimeKind.Local).AddTicks(6218), true, 25, "Aptent aenean at commodo amet id enim sagittis blandit luctus a vestibulum neque", "Vulputate ullamcorper lacus", 256, "sagittis-pulvinar-orci", 139 },
                    { 5, 4, null, "Tempor, felis vestibulum, quis tristique nostra, eget dignissim molestie finibus, suspendisse nec dolor dui, placerat, pretium. Nunc, eleifend adipiscing fringilla, ut lorem, quis nunc non, vulputate sagittis elementum id, curabitur dignissim praesent feugiat. Et rutrum amet massa platea volutpat lorem, aliquam lacus adipiscing tempus elit, diam magna, felis sed maximus nullam. Sed, lorem, mauris curabitur nisi, ultricies facilisis placerat placerat, elit, finibus, dui adipiscing laoreet. Tempus primis donec nisi, litora auctor nibh porta at ultrices, mi sem, mauris urna, sit.", new DateTime(2023, 8, 31, 17, 31, 15, 345, DateTimeKind.Local).AddTicks(7540), true, 18, "Eros volutpat mollis augue eu luctus mi nostra vulputate at blandit integer urna", "Id pellentesque luctus", 184, "feugiat-dapibus-conubia", 215 },
                    { 6, 1, null, "Libero pharetra per enim enim, sollicitudin tortor lorem aliquam odio pellentesque. In dui, semper volutpat, convallis magna, suscipit metus consectetur odio, bibendum inceptos nibh erat. Fames neque lacinia ultrices eros neque, vel maximus varius, ante, interdum vel, nisi sed malesuada purus. Etiam imperdiet in, rhoncus justo ultrices, aliquet tempor posuere, lacinia auctor volutpat. Tempor, tristique quis, ante, mi varius at, nullam dictumst class augue ante pretium non, aptent blandit fermentum.", new DateTime(2023, 9, 6, 12, 16, 15, 345, DateTimeKind.Local).AddTicks(8844), true, 12, "Lacinia volutpat sed aenean consequat ex efficitur semper blandit nibh himenaeos primis sit", "Quisque massa nisi", 276, "tortor-vestibulum-metus", 284 },
                    { 7, 1, null, "Primis enim tellus hendrerit nostra, nullam commodo nibh porttitor iaculis vulputate. Ligula eu, ut curabitur auctor, metus porta auctor id, eros magna ac. Luctus, lacinia, euismod, odio, sociosqu himenaeos id, nibh, sagittis ligula, nec, posuere, varius, gravida. Fusce tempor dignissim erat phasellus placerat varius vulputate nam mauris, est tellus. Mattis amet fames mi eget magna, quisque gravida rhoncus, venenatis dolor sociosqu sit risus.", new DateTime(2023, 8, 30, 9, 29, 15, 346, DateTimeKind.Local).AddTicks(85), true, 15, "Vestibulum odio ligula quis himenaeos sagittis taciti cursus aenean dolor quam litora aliquet", "Nec mi dictumst", 119, "neque-ligula-porta", 194 },
                    { 8, 2, null, "Mattis, bibendum, venenatis quam, curabitur odio volutpat libero orci vulputate fermentum sed sem, ut maximus. Litora conubia himenaeos placerat, adipiscing venenatis dui pellentesque et maximus efficitur nunc leo. Imperdiet quis tempor cras mauris, feugiat, a, consectetur accumsan commodo ante integer cursus, dolor inceptos. Tellus augue adipiscing justo dui, sapien massa himenaeos maximus commodo, sollicitudin cras sed, vel ornare id, eget.", new DateTime(2023, 9, 6, 21, 37, 15, 346, DateTimeKind.Local).AddTicks(1189), true, 28, "Feugiat dui eros hac nisi quis ultrices lacinia diam placerat sed augue finibus", "Praesent mattis non", 171, "ante-placerat-lorem", 139 },
                    { 9, 1, null, "Id, elit ultricies magna aliquet bibendum, sodales phasellus libero euismod, quam. Tellus, laoreet nibh bibendum nec, vulputate posuere, molestie nisi at, in, in nulla eleifend, quisque lorem, varius lectus dolor. Nostra, commodo, feugiat lobortis praesent aliquet quis, nec orci, at, per convallis accumsan eget nulla etiam id, platea a. Varius est porta, integer bibendum, aenean dui purus volutpat, finibus, vitae. Odio auctor risus posuere, ante elit, dictumst ante, a, dui, habitasse commodo. Et, interdum, sociosqu convallis massa, nunc odio, eros volutpat ut orci, porttitor.", new DateTime(2023, 8, 29, 1, 47, 15, 346, DateTimeKind.Local).AddTicks(2620), true, 10, "Tempor libero id eget venenatis mi mi efficitur praesent felis tortor lorem posuere", "Tempor euismod sapien", 119, "luctus-maximus-efficitur", 271 },
                    { 10, 3, null, "Porttitor vivamus nullam vulputate vel placerat, ultrices, bibendum nunc, nulla, eros, lectus. Erat ultrices vivamus facilisis magna, convallis odio tristique morbi posuere, sagittis nulla, eleifend, imperdiet vel, diam. Bibendum vitae, donec mattis, mattis lobortis vehicula dictum quam cursus, efficitur primis. Risus ligula, ante magna, quisque litora porta, posuere commodo, conubia velit congue, in, etiam vel. Ligula enim, mauris dignissim massa a, conubia arcu lectus, interdum, finibus eros. Lectus nibh, ornare non, turpis pretium lacinia et, bibendum, fames pellentesque magna, ante tristique consectetur massa. Cursus lacinia lectus pellentesque placerat ultricies rhoncus molestie erat facilisis dictum.", new DateTime(2023, 8, 29, 23, 31, 15, 346, DateTimeKind.Local).AddTicks(4189), true, 23, "Congue vehicula varius eleifend nam sagittis posuere tristique enim ac arcu quisque accumsan", "Enim porta sem", 117, "aptent-vivamus-fusce", 112 }
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "5833bba7-8820-4163-b779-b36d239ad574", "733a30f4-82e2-4302-b3d4-d0e736d746ed" },
                    { "0cdf28f2-e109-4360-9c5e-b8a4df66091e", "85f7dca3-ed32-49e7-bf16-f49e3bb8ae04" },
                    { "a434f075-570e-44f9-82f8-62cd88f5eb0a", "a9d1613a-6655-4ff5-bedf-313bf30093c1" }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "CommentHeader", "CommentText", "CommentTime", "Email", "Name", "PostId" },
                values: new object[,]
                {
                    { 1, "Donec placerat amet", "Elementum in, lectus dictumst id feugiat amet, egestas volutpat enim, magna, pretium nec. In, ullamcorper diam augue ligula, et at at, dui, libero quis, praesent pharetra posuere, id sapien vehicula.", new DateTime(2023, 9, 6, 2, 48, 15, 346, DateTimeKind.Local).AddTicks(9844), "elementum@sem.com", "Metus orci", 1 },
                    { 2, "Curabitur gravida adipiscing", "Ante neque augue pretium euismod, massa magna fames volutpat, amet.", new DateTime(2023, 9, 7, 6, 1, 15, 347, DateTimeKind.Local).AddTicks(834), "ut@fringilla.com", "Inceptos molestie", 8 },
                    { 3, "Non blandit vel", "Dui laoreet ultrices et, dictum nec elit, enim faucibus leo tristique sit est. Rutrum eleifend imperdiet nisi blandit, iaculis posuere, suspendisse aenean convallis class euismod ultrices.", new DateTime(2023, 8, 29, 2, 39, 15, 347, DateTimeKind.Local).AddTicks(1975), "semper@mi.com", "Tortor in", 1 },
                    { 4, "Eu morbi sed", "Blandit, dui nisl urna, conubia facilisis cursus proin amet, pulvinar torquent interdum placerat, hendrerit fusce congue.", new DateTime(2023, 9, 5, 5, 31, 15, 347, DateTimeKind.Local).AddTicks(2937), "et@sociosqu.com", "Taciti bibendum", 2 },
                    { 5, "Commodo rhoncus congue", "Sit ante dui morbi consectetur mauris, sociosqu imperdiet bibendum, pulvinar, mi id aliquet sapien.", new DateTime(2023, 8, 30, 0, 48, 15, 347, DateTimeKind.Local).AddTicks(3920), "accumsan@sit.com", "Neque mi", 1 },
                    { 6, "Metus imperdiet et", "Quisque quis fames nibh, porttitor, amet, neque, rutrum nulla nunc, malesuada in, vestibulum tortor, hac dictum condimentum ligula.", new DateTime(2023, 9, 5, 9, 28, 15, 347, DateTimeKind.Local).AddTicks(4888), "imperdiet@torquent.com", "Class volutpat", 6 },
                    { 7, "Duis ante urna", "Nullam sollicitudin faucibus purus arcu, quis ante, in lobortis dui, pharetra viverra sociosqu nulla, ultrices fermentum placerat, ornare litora.", new DateTime(2023, 9, 6, 4, 18, 15, 347, DateTimeKind.Local).AddTicks(5861), "posuere@luctus.com", "Eleifend ac", 3 },
                    { 8, "Neque nec consectetur", "Sodales per orci, enim urna molestie nulla elementum ad sed arcu, dolor ultricies vitae et posuere mollis enim, auctor. Risus rhoncus bibendum, ultricies mattis hendrerit rhoncus, purus iaculis fringilla, facilisis aptent.", new DateTime(2023, 9, 1, 15, 12, 15, 347, DateTimeKind.Local).AddTicks(7010), "facilisis@eleifend.com", "Dolor ante", 4 },
                    { 9, "Nec massa id", "Vivamus luctus bibendum, hac vel, nec, lobortis ante, quam, litora justo imperdiet commodo pulvinar. Orci elit maecenas vestibulum litora quisque augue accumsan ante, nec, dui, laoreet, imperdiet faucibus nibh, sem, lacinia.", new DateTime(2023, 9, 6, 19, 7, 15, 347, DateTimeKind.Local).AddTicks(8147), "rhoncus@nunc.com", "Curabitur nibh", 3 },
                    { 10, "Massa mi ligula", "Risus consectetur leo taciti mattis, feugiat et, tempor pharetra posuere ullamcorper. Blandit litora facilisis tortor dictum odio sed, tempus et, ante, massa bibendum fames fusce laoreet ullamcorper leo.", new DateTime(2023, 9, 5, 20, 45, 15, 347, DateTimeKind.Local).AddTicks(9316), "urna@fringilla.com", "Pretium fringilla", 3 }
                });

            migrationBuilder.InsertData(
                table: "GalleryImages",
                columns: new[] { "Id", "CreatedBy", "CreatedDate", "Description", "ImageUrl", "PostId", "Title" },
                values: new object[,]
                {
                    { 1, "Tran Van Hanh", new DateTime(2023, 9, 7, 18, 50, 15, 347, DateTimeKind.Local).AddTicks(9868), "Introduction programming C#", "~/assets/img/Introduction-to-C.jpg", 5, "Learn C#" },
                    { 2, "Fabrizio Romano", new DateTime(2023, 9, 7, 18, 50, 15, 347, DateTimeKind.Local).AddTicks(9872), "Messi Winner Football World Cup 2022", "~/assets/img/footballworlcup.jpg", 1, "Football World Cup" },
                    { 3, "Anh Quan", new DateTime(2023, 9, 7, 18, 50, 15, 347, DateTimeKind.Local).AddTicks(9874), "Learn basic", "~/assets/img/programming.jpg", 7, "Introduction to Programming" },
                    { 4, "Ta Van Toan", new DateTime(2023, 9, 7, 18, 50, 15, 347, DateTimeKind.Local).AddTicks(9875), "Supper WEB", "~/assets/img/Web-Development-Trends.jpg", 4, "WEB trending" },
                    { 5, "Pham Thi Len", new DateTime(2023, 9, 7, 18, 50, 15, 347, DateTimeKind.Local).AddTicks(9900), "Description for Image 1", "~/assets/img/Halong-Main-picture.jpg", 1, "Ha Long Bay" },
                    { 6, "Tran Huy Tho", new DateTime(2023, 9, 7, 18, 50, 15, 347, DateTimeKind.Local).AddTicks(9903), "Supper likes", "~/assets/img/food_3.jpg", 9, "Food in Viet Nam" }
                });

            migrationBuilder.InsertData(
                table: "PostTagMaps",
                columns: new[] { "PostId", "TagId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 4 },
                    { 5, 5 },
                    { 6, 6 },
                    { 7, 7 },
                    { 8, 8 },
                    { 9, 9 },
                    { 10, 10 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_GalleryImages_PostId",
                table: "GalleryImages",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_CategoryId",
                table: "Posts",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PostTagMaps_TagId",
                table: "PostTagMaps",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaims_RoleId",
                table: "RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Roles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "GalleryImages");

            migrationBuilder.DropTable(
                name: "PostTagMaps");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
