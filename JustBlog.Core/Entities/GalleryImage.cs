﻿namespace JustBlog.Core.Entities
{
    public class GalleryImage
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}
