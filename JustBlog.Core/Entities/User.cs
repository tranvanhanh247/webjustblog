﻿using Microsoft.AspNetCore.Identity;

namespace JustBlog.Core.Entities
{
    public class User : IdentityUser
    {
        [PersonalData]
        public int Age { get; set; }
        [PersonalData]
        public string AboutMe { get; set; }
    }
}
