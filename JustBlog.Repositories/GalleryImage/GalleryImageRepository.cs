﻿using JustBlog.Core.Database;
using JustBlog.Repositories.Infrastructure;

namespace JustBlog.Repositories.GalleryImage
{
    public class GalleryImageRepository : GenericRepository<Core.Entities.GalleryImage>, IGalleryImageRepository
    {
        public GalleryImageRepository(JustBlogContext Context) : base(Context) { }

        public Core.Entities.GalleryImage GetImageByName(string name)
        {
            return Context.GalleryImages.FirstOrDefault(c => c.Title.Trim().ToLower() == name.Trim().ToLower())!;
        }

        public List<Core.Entities.GalleryImage> GetImageByPost(string postTitle)
        {
            return Context.GalleryImages
                .Join(Context.Posts, c => c.PostId, p => p.Id, (c, p) => new { GalleryImage = c, Post = p })
                .Where(cp => cp.Post.Title.Trim().ToLower() == postTitle.Trim().ToLower())
                .Select(cp => cp.GalleryImage)
                .ToList();
        }
    }
}
