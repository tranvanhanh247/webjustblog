﻿using JustBlog.Repositories.Infrastructure;

namespace JustBlog.Repositories.GalleryImage
{
    public interface IGalleryImageRepository : IGenericRepository<Core.Entities.GalleryImage>
    {
        /// <summary>
        /// get Image by post title
        /// </summary>
        /// <param name="postTitle"></param>
        /// <returns></returns>
        List<Core.Entities.GalleryImage> GetImageByPost(string postTitle);

        /// <summary>
        /// get image by name image
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Core.Entities.GalleryImage GetImageByName(string name);
    }
}
