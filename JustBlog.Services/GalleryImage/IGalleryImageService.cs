﻿using JustBlog.ViewModels.GalleryImage;

namespace JustBlog.Services.GalleryImage
{
    public interface IGalleryImageService
    {
        /// <summary>
        /// Retrieves all images.
        /// </summary>
        /// <returns>A list of GalleryViewModel objects representing the image.</returns>
        IList<GalleryViewModel> GetAllImages();

        /// <summary>
        /// Retrieves Images for a specific post.
        /// </summary>
        /// <param name="postTitle">The title of the post.</param>
        /// <returns>A list of ImageViewModel objects representing the Images.</returns>
        IList<GalleryViewModel> GetImagesByPost(string postTitle);

        /// <summary>
        /// Adds a new Image.
        /// </summary>
        /// <param name="ImageToCreate">The ImageToCreateViewModel object containing the data for the new Image.</param>
        /// <returns>True if the addition was successful, otherwise false.</returns>
        bool AddImage(GalleryToCreateViewModel ImageToCreate);

        /// <summary>
        /// Retrieves a specific page of Images.
        /// </summary>
        /// <param name="page">The page number.</param>
        /// <param name="pageSize">The number of Images per page.</param>
        /// <returns>A list of ImageViewModel objects representing the Images on the specified page.</returns>
        IList<GalleryViewModel> GetPagedImages(int page, int pageSize);

        /// <summary>
        /// Retrieves the total count of all Images.
        /// </summary>
        /// <returns>The total count of Images.</returns>
        int CountAllImages();

        /// <summary>
        /// Deletes a Image by its ID.
        /// </summary>
        /// <param name="id">The ID of the Image to delete.</param>
        /// <returns>True if the deletion was successful, otherwise false.</returns>
        bool DeleteImage(int id);

        /// <summary>
        /// Retrieves a ImageToUpdateViewModel object for editing a Image based on its ID.
        /// </summary>
        /// <param name="id">The ID of the Image to retrieve.</param>
        /// <returns>The ImageToUpdateViewModel object for editing the Image, or null if not found.</returns>
        GalleryToUpdateViewModel GetImageToUpdate(int id);

        /// <summary>
        /// Updates an existing Image.
        /// </summary>
        /// <param name="ImageToUpdate">The ImageToUpdateViewModel object containing the updated data for the Image.</param>
        /// <returns>True if the update was successful, otherwise false.</returns>
        bool UpdateImage(GalleryToUpdateViewModel ImageToUpdate);

        /// <summary>
        /// Retrieves the details of a Image based on its ID.
        /// </summary>
        /// <param name="id">The ID of the Image to retrieve.</param>
        /// <returns>The ImageDetailsViewModel object representing the details of the Image, or null if not found.</returns>
        GalleryDetailsViewModel GetImageDetails(int id);
    }
}
