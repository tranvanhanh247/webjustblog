﻿using AutoMapper;
using JustBlog.Repositories.Infrastructure;
using JustBlog.ViewModels.GalleryImage;
using Microsoft.Extensions.Logging;

namespace JustBlog.Services.GalleryImage
{
    public class GalleryImageService : IGalleryImageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        public GalleryImageService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<GalleryImageService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public IList<GalleryViewModel> GetImagesByPost(string postTitle)
        {
            try
            {
                var Images = _unitOfWork.GalleryImageRepository.GetImageByPost(postTitle);
                return Images.OrderByDescending(c => c.Id).Select(c => _mapper.Map<GalleryViewModel>(c)).ToList();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null!;
            }
        }
        public bool AddImage(GalleryToCreateViewModel ImageToCreate)
        {
            var image = new Core.Entities.GalleryImage
            {
                Title = ImageToCreate.Title,
                Description = ImageToCreate.Description,
                ImageUrl = ImageToCreate.ImageUrl,
            };

            try
            {
                _unitOfWork.GalleryImageRepository.Insert(image);
                _unitOfWork.Save();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return false;
            }
        }

        public IList<GalleryViewModel> GetPagedImages(int page, int pageSize)
        {
            try
            {
                return _unitOfWork.GalleryImageRepository.GetPagedItems(page, pageSize).Select(c => _mapper.Map<GalleryViewModel>(c)).ToList();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null!;
            }
        }

        public int CountAllImages()
        {
            try
            {
                return _unitOfWork.GalleryImageRepository.CountAll();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return 0;
            }
        }

        public bool DeleteImage(int id)
        {
            try
            {
                _unitOfWork.GalleryImageRepository.DeleteById(id);
                _unitOfWork.Save();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return false;
            }
        }

        public GalleryToUpdateViewModel GetImageToUpdate(int id)
        {
            try
            {
                return _mapper.Map<GalleryToUpdateViewModel>(_unitOfWork.GalleryImageRepository.GetById(id));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null!;
            }
        }

        public bool UpdateImage(GalleryToUpdateViewModel ImageToUpdate)
        {

            try
            {
                var Image = _mapper.Map<Core.Entities.GalleryImage>(ImageToUpdate);
                _unitOfWork.GalleryImageRepository.Update(Image);
                _unitOfWork.Save();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return false;
            }
        }

        public GalleryDetailsViewModel GetImageDetails(int id)
        {
            try
            {
                return _mapper.Map<GalleryDetailsViewModel>(_unitOfWork.GalleryImageRepository.GetById(id));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null!;
            }
        }

        public IList<GalleryViewModel> GetAllImages()
        {
            try
            {
                return _unitOfWork.GalleryImageRepository.GetAll().Select(p => _mapper.Map<GalleryViewModel>(p)).ToList();
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return null!;
            }
        }
    }
}
