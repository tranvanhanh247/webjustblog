﻿using JustBlog.Services.GalleryImage;
using JustBlog.Services.Post;
using JustBlog.ViewModels.GalleryImage;
using JustBlog.ViewModels.Others;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JustBlog.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class GalleryController : Controller
    {
        private readonly IGalleryImageService _galleryService;
        private readonly IPostService _postService;

        public GalleryController(IGalleryImageService galleryService, IPostService postService)
        {
            _galleryService = galleryService;
            _postService = postService;
        }

        [Authorize(policy: "Get")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(policy: "Get")]
        public IActionResult GetPagedGallery(int page, int pageSize)
        {
            var images = _galleryService.GetPagedImages(page, pageSize);
            var total = _galleryService.CountAllImages();
            var dataTable = new DataTableViewModel
            {
                Action = "GetPagedImages",
                Controller = "Gallery",
                Total = total,
                Page = page,
                LastPage = (int)Math.Ceiling((double)total / pageSize),
                PageSize = pageSize,
                Columns = new string[] { "Id", "Title", "Description", "ImageUrl", "Create Date", "Created By" },
                Data = images.Select(image =>
                    new Dictionary<string, string>
                    {
                        { "Id", image.Id.ToString() },
                        { "Title",image.Title.ToString() },
                        { "Description", image.Description },
                        { "ImageUrl", image.ImageUrl },
                        { "Created Date", image.CreatedDate.ToString() },
                        { "Created By", image.CreatedBy.ToString() }
                    }
                ).ToList()
            };
            return PartialView("_DataTablePartial", dataTable);
        }

        [Authorize(policy: "Get")]
        public IActionResult Details(int id)
        {
            var ImageDetails = _galleryService.GetImageDetails(id);
            return View(ImageDetails);
        }

        [Authorize(policy: "Create Or Edit")]
        public IActionResult Create()
        {
            ViewBag.Posts = _postService.GetAllPosts();
            return View();
        }

        [Authorize(policy: "Create Or Edit")]
        [HttpPost]
        public IActionResult Create(GalleryToCreateViewModel ImageToCreate)
        {
            if (ModelState.IsValid && _galleryService.AddImage(ImageToCreate))
                return Redirect("/Admin/Image");
            ViewBag.Posts = _postService.GetAllPosts();
            return View(ImageToCreate);

        }

        [Authorize(policy: "Create Or Edit")]
        public IActionResult Edit(int id)
        {
            var editImage = _galleryService.GetImageToUpdate(id);
            if (editImage == null)
                return View("NotFound");

            ViewBag.Posts = _postService.GetAllPosts();
            return View(editImage);
        }

        [Authorize(policy: "Create Or Edit")]
        [HttpPost]
        public IActionResult Edit(GalleryToUpdateViewModel ImageToUpdate)
        {
            if (ModelState.IsValid)
            {
                if (_galleryService.UpdateImage(ImageToUpdate))
                    ViewBag.Message = "Update Image successfully";
                else
                    ViewBag.Message = "Update Image failed";
            }

            ViewBag.Posts = _postService.GetAllPosts();
            return View(ImageToUpdate);
        }

        [Authorize(policy: "Delete")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            if (_galleryService.DeleteImage(id))
                return StatusCode(200);
            return View("NotFound");
        }
    }
}
