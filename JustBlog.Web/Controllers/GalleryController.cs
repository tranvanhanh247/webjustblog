﻿using JustBlog.Services.GalleryImage;
using Microsoft.AspNetCore.Mvc;

namespace JustBlog.Web.Controllers
{
    public class GalleryController : Controller
    {
        private readonly IGalleryImageService _galleryImageService;

        public GalleryController(IGalleryImageService galleryImageService)
        {
            _galleryImageService = galleryImageService;
        }

        public IActionResult Index()
        {
            var images = _galleryImageService.GetAllImages();
            return View(images.ToList());
        }
    }
}
