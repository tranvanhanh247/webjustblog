﻿using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.GalleryImage
{
    public class GalleryToUpdateViewModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Gallery title must be required")]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description must be required")]
        public string Description { get; set; }

        public int PostId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Image Url must be required")]
        public string ImageUrl { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Created Date text must be required")]
        public DateTime CreatedDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Creater text must be required")]
        public string CreatedBy { get; set; }
    }
}
