﻿namespace JustBlog.ViewModels.GalleryImage
{
    public class GalleryDetailsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Post { get; set; }
    }
}
