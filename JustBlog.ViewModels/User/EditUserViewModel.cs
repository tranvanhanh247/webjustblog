﻿using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.User
{
    public class EditUserViewModel
    {
        public string Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public int Age { get; set; }
        [Required]
        public string AboutMe { get; set; }
        public IList<string> RoleIds { get; set; }
    }
}
