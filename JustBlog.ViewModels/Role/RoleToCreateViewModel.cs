﻿using System.ComponentModel.DataAnnotations;

namespace JustBlog.ViewModels.Role
{
    public class RoleToCreateViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}
